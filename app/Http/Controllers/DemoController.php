<?php

namespace App\Http\Controllers;

use App\Models\TodoList;
use Illuminate\Http\Request;

class DemoController extends Controller
{
    public function voir(Request $request, $id){

        return view("demo", compact("id"));
    }

    public function about(){
        
        return view("about");
    }
}

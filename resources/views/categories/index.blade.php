@extends("layout")

@section('title', "La liste des categories")

@section('content')<
    <h3>La liste des articles</h3>
    <div class="d-grid gap-2 d-md-flex justify-content-md-end">
        <a href="{{ route('categories.create') }}" class="btn btn-primary">
            Ajouter une nouvelle catégorie d'article
        </a>
    </div>
    @if (Session::has('success'))
        <div class="alert alert-success">
            {{ Session::get('success') }}
        </div>
    @endif

    @if (Session::has('failed'))
        <div class="alert alert-danger">
            {{ Session::get('failed') }}
        </div>
    @endif
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Nom de la catégories</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($categories as $category)
                <tr>
                    <td>{{ $category->name }}</td>
                    <td class="d-inline-flex gap-1">
                        <a class="btn btn-warning btn-sm" 
                        href="{{ route('categories.edit',$category->id) }}">
                            Modifier
                        </a>
                        <form action="{{ route("categories.destroy", $category->id) }}" 
                                method="post">
                                @csrf
                                @method("DELETE")
                                <button onclick="return confirm('Êtes-vous sûr ?')"
                                type="submit" class="btn btn-danger btn-sm">
                                    Supprimer
                                </button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <div class="position-relative mt-5">
        <div class="position-absolute top-0 start-50 translate-middle">
             {{ $categories->links() }}
        </div>
    </div>

@endsection